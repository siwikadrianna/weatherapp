# Table of Contents
1. [Description](#description)
2. [Getting started](#getting-started)
3. [Usage](#usage)
4. [Arhitecture](#arhitecture)
5. [Dependencies](#dependencies)
6. [Design](#design)
7. [API](#api)

# WeatherApp
Application created for recruitment purposes. 

# Description
<p>WeatherApp displays current weather and forecast (5 days/3 hours) for current or searched location.</p>

# Getting started
<p>
1. Make sure you have the Xcode version 14.3 or above installed on your computer.<br>
2. Download the WeatherApp project files from the repository.<br>
3. Open the project files in Xcode.<br>
4. Run the active scheme.<br>
Location permission should be displayd. You should see weather forecast for your current location or for Miami (depends on permissions).<br>

# Usage
* Allow app to use location,
* Check weather forecast,
* Search for different locations.

# Architecture
* WeatherApp project is implemented using the <strong>Model-View-ViewModel (MVVM)</strong> architecture pattern.
* Project doesn't have a database.
* Project uses SPM.

# Dependencies
Swift Package Manager ([SPM](https://swift.org/package-manager/)) is used as a dependency manager.
List of dependencies: 
* [lottie-ios](https://github.com/airbnb/lottie-ios) -> cross-platform library for iOS, macOS, tvOS, Android, and Web that natively renders vector-based animations and art in realtime with minimal code.

# Design
* [Lottie Animations](https://lottiefiles.com/vdr0uy2wwsoljqtc)

# API 
* [Weather API](https://openweathermap.org)
