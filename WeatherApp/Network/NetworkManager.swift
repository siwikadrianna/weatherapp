//
//  NetworkManager.swift
//  WeatherApp
//
//  Created by Adrianna Siwik on 08/05/2023.
//

import Combine
import Foundation

final class NetworkManager<T: Codable> {
    func fetch(for url: URL) -> AnyPublisher<T, NetworkError> {
        URLSession.shared
            .dataTaskPublisher(for: url)
            .tryMap { element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse,
                      httpResponse.statusCode == 200 else {
                    throw URLError(.badServerResponse)
                }
                _ = String(data: element.data, encoding: .utf8)!
                return element.data
            }
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError({(error) -> NetworkError in
                return NetworkError.decodingError(error: error.localizedDescription)
            })
            .eraseToAnyPublisher()
    }
}

enum NetworkError: Error {
    case invalidData, invalidResponse, error(error: String), decodingError(error: String)
}
