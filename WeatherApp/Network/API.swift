//
//  API.swift
//  WeatherApp
//
//  Created by Adrianna Siwik on 08/05/2023.
//

import Foundation

struct API {
    static let key = "d8abf2d78b3a3387337807338b342580"
}

extension API {
    static let baseURLString = "https://api.openweathermap.org/data/2.5/"
    static func getURLFor(lat: Double, lon: Double) -> String {
        return "\(baseURLString)forecast?lat=\(lat)&lon=\(lon)&exclude=minutely&appid=\(key)&units=metric"
    }
}
