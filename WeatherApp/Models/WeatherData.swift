//
//  WeatherData.swift
//  WeatherApp
//
//  Created by Adrianna Siwik on 07/05/2023.
//

import Foundation

class WeatherModel : ObservableObject {
    @Published var list: [WeatherResponse]
    @Published var city: City
    
    init(list: [WeatherResponse], city: City) {
        self.list = list
        self.city = city
    }
    
    static var defaultWeather: WeatherModel {
        WeatherModel(list: [WeatherResponse](repeating: WeatherResponse(), count: 5), city: City())
    }
}

struct WeatherResponseFirst: Codable {
    var list: [WeatherResponse]
    var city: City
}

struct City: Codable {
    var name: String
    var sunrise: Int
    var sunset: Int
    
    init() {
        name = "Miami"
        sunrise = 0
        sunset = 0
    }
}

struct WeatherResponse: Codable, Identifiable {
    var dt: Int
    var main: Weather
    var weather: [WeatherDetail]
    var wind: Wind
    var pop: Double
    
    init() {
        dt = 0
        main = Weather()
        weather = []
        wind = Wind()
        pop = 0.0
    }
}

struct Weather: Codable, Identifiable {
    var temp: Double
    var feels_like: Double
    var temp_min: Double
    var temp_max: Double
    var pressure: Int
    var humidity: Int
    
    init() {
        temp = 0.0
        feels_like = 0.0
        temp_min = 0.0
        temp_max = 0.0
        pressure = 0
        humidity = 0
    }
}

extension Weather {
    var id: UUID {
        return UUID()
    }
}

struct WeatherDetail: Codable {
    var main: String
    var description: String
    var icon: String
    
    init() {
        main = "Sunny"
        description = "Sunny"
        icon = "sun.max"
    }
}

struct Wind: Codable {
    var speed: Double
    
    init() {
        speed = 0.0
    }
}

extension WeatherResponse {
    var id: UUID {
        return UUID()
    }
}
