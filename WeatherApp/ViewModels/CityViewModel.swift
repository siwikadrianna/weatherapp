//
//  CityViewModel.swift
//  WeatherApp
//
//  Created by Adrianna Siwik on 08/05/2023.
//

import Combine
import CoreLocation
import Foundation
import SwiftUI

final class CityViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    @Published var weather = WeatherModel.defaultWeather
    @Published var city = "Miami"
    
    private let networkManager = NetworkManager<WeatherResponseFirst>()
    private var subscription = Set<AnyCancellable>()
    
    override init() {
        super.init()
        Task { await fetchWeather() }
    }
    
    private let locationManager = CLLocationManager()
    
    private lazy var dateFormatter: DateFormatter =  {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        return formatter
    }()
    
    private lazy var dayFormatter: DateFormatter =  {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE"
        return formatter
    }()
    
    private lazy var hourFormatter: DateFormatter =  {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh a"
        return formatter
    }()
    
    private lazy var timeFormatter: DateFormatter =  {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        return formatter
    }()
    
    var date: String {
        return dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(weather.list[0].dt)))
    }
    
    var sunriseTime: String {
        return timeFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(weather.city.sunrise)))
    }
    
    var sunsetTime: String {
        return timeFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(weather.city.sunset)))
    }
    
    var weatherIcon: String {
        if !weather.list.isEmpty {
            return !weather.list[0].weather.isEmpty ? weather.list[0].weather[0].icon : "sun.fill"
        }
        return "sun.max.fill"
    }
    
    var temperature: String {
        return getTempFor(temp: weather.list[0].main.temp)
    }
    
    var conditions: String {
        if !weather.list.isEmpty {
            return !weather.list[0].weather.isEmpty ? weather.list[0].weather[0].main : "Sunny"
        }
        return ""
    }
    
    var windSpeed: String {
        return String(format: "%0.1fkm/h", !weather.list.isEmpty ? weather.list[0].wind.speed : "0")
    }
    
    var humidity: String {
        return String(format: "%d%%", weather.list[0].main.humidity)
    }
    
    var rainChances: String {
        return String(format: "%0.0f%%", weather.list[0].pop)
    }
    
    func getTempFor(temp: Double) -> String {
        return String(format: "%0.1f°C", temp)
    }
    
    func getDayFor(timestamp: Int) -> String {
        dayFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(timestamp)))
    }
    
    func getHourFor(timestamp: Int) -> String {
        hourFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(timestamp)))
    }
    
    func requestLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        guard locationManager.authorizationStatus == .authorizedWhenInUse || locationManager.authorizationStatus == .authorizedAlways else {
            getLocation(for: city)
            return
        }
        
        Task { await fetchWeather() }
    }
    
    private func fetchWeather() async {
        guard let currentLocation = locationManager.location else {
            requestLocation()
            return
        }
        getWeather(coord: currentLocation.coordinate)
    }
    
    func getLocation(for city: String) {
        CLGeocoder().geocodeAddressString(city) { [weak self] places, error in
            if let places = places,
               let place =  places.first  {
                self?.city = city
                self?.getWeather(coord: place.location?.coordinate)
            }
        }
    }
    
    private func getWeather(coord: CLLocationCoordinate2D?) {
        if let coord = coord {
            let urlString = API.getURLFor(lat: coord.latitude, lon: coord.longitude)
            getWeatherInternal(for: urlString)
        } else {
            let urlString = API.getURLFor(lat: 30.12, lon: 100.12) // Random location
            getWeatherInternal(for: urlString)
        }
    }
    
    private func getWeatherInternal(for urlString: String) {
        networkManager.fetch(for: URL(string: urlString)!)
            .map {
                return WeatherModel(list: $0.list, city: $0.city)
            }
            .replaceError(with: WeatherModel.defaultWeather)
            .receive(on: DispatchQueue.main)
            .assign(to: \.weather, on: self)
            .store(in: &subscription)
    }
    
    func getLottieAnimationFor(icon: String) -> String {
        switch icon {
        case "01d":
            return "weather-sunny"
        case "02d":
            return "weather-partly-cloudy"
        case "03d", "04d", "03n", "04n":
            return "weather-windy"
        case "09d", "10d":
            return "weather-partly-shower"
        case "11d", "11n":
            return "weather-storm"
        case "13d":
            return "weather-snow-sunny"
        case "50d":
            return "foggy"
        case "01n":
            return "weather-night"
        case "02n":
            return "weather-cloudynight"
        case "09n", "10n":
            return "weather-rainynight"
        case "13n":
            return "weather-snownight"
        case "50n":
            return "weather-mist"
        default:
            return "weather-sunny"
        }
    }
    
    func getWeatherIconFor(icon: String) -> Image {
        switch icon {
        case "01d":
            return Image(systemName: "sun.max.fill")
        case "02d":
            return Image(systemName: "cloud.sun.fill")
        case "03d", "04d", "03n", "04n":
            return Image(systemName: "cloud.fill")
        case "09d", "09n":
            return Image(systemName: "cloud.drizzle.fill")
        case "10d":
            return Image(systemName: "cloud.sun.rain.fill")
        case "11d":
            return Image(systemName: "cloud.bolt.rain.fill")
        case "13d", "13n":
            return Image(systemName: "cloud.snow.fill")
        case "50d", "50n":
            return Image(systemName: "cloud.fog.fill")
        case "01n":
            return Image(systemName: "moon.fill")
        case "02n":
            return Image(systemName: "cloud.moon.fill")
        case "10n":
            return Image(systemName: "cloud.moon.rain.fill")
        case "11n":
            return Image(systemName: "cloud.moon.bolt.fill")
        default:
            return Image(systemName: "sun.max.fill")
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        requestLocation()
    }
}
