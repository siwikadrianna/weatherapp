//
//  WeatherAppApp.swift
//  WeatherApp
//
//  Created by Adrianna Siwik on 07/05/2023.
//

import SwiftUI

@main
struct WeatherAppApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
