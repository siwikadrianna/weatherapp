//
//  WeatherTodayView.swift
//  WeatherApp
//
//  Created by Adrianna Siwik on 07/05/2023.
//

import SwiftUI

struct WeatherTodayView: View {
    @ObservedObject var cityViewModel: CityViewModel
    
    var body: some View {
        VStack(spacing: 15) {
            HStack(spacing: 25) {
                LottieView(name: cityViewModel.getLottieAnimationFor(icon: cityViewModel.weatherIcon))
                    .frame(width: 100, height: 100)
                
                VStack(alignment: .leading, spacing: 5) {
                    Text("\(cityViewModel.temperature)")
                        .font(.system(size: 42))
                    Text(cityViewModel.conditions)
                }
            }
            
            HStack {
                Spacer()
                widgetView(image: "wind", color: .green, title: "\(cityViewModel.windSpeed)")
                Spacer()
                widgetView(image: "drop.fill", color: .blue, title: "\(cityViewModel.humidity)")
                Spacer()
                widgetView(image: "umbrella", color: .red, title: "\(cityViewModel.rainChances)")
                Spacer()
            }
            
            HStack {
                Spacer()
                widgetViewSun(image: "sunrise.fill", title: "Sunrise", time: cityViewModel.sunriseTime)
                Spacer()
                widgetViewSun(image: "sunset.fill", title: "Sunset", time: cityViewModel.sunsetTime)
                Spacer()
            }
        }
        .padding()
        .background(RoundedRectangle(cornerRadius: 20)
            .fill(LinearGradient(gradient: Gradient(colors: [Color.cyan.opacity(0.5), Color.cyan]),
                                 startPoint: .top,
                                 endPoint: .bottom)).opacity(0.3))
        .shadow(color: .white.opacity(0.1), radius: 2, x: -2, y: -2)
        .shadow(color: .black.opacity(0.2), radius: 2, x: 2, y: 2)
    }
    
    private func widgetView(image: String, color: Color, title: String) -> some View {
        VStack {
            Image(systemName: image)
                .padding()
                .font(.title)
                .foregroundColor(color)
                .background(RoundedRectangle(cornerRadius: 10)
                    .fill(Color.white)
                    .frame(width: 60, height: 60))
            Text(title)
        }
    }
    
    private func widgetViewSun(image: String, title: String, time: String) -> some View {
        VStack(spacing: 5) {
            Text(title)
            Image(systemName: image)
                .font(.title)
                .foregroundColor(.yellow)
            Text(time)
        }
        .padding()
    }
}

struct WeatherTodayView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherTodayView(cityViewModel: CityViewModel())
    }
}
