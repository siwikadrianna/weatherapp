//
//  MainView.swift
//  WeatherApp
//
//  Created by Adrianna Siwik on 08/05/2023.
//

import SwiftUI

struct MainView: View {
    @ObservedObject var cityViewModel = CityViewModel()
    
    var body: some View {
        ZStack(alignment: .bottom) {
            VStack {
                SearchBarView(cityViewModel: cityViewModel)
                ScrollView(showsIndicators: false) {
                    VStack {
                        WeatherSectionView(title: cityViewModel.weather.city.name,
                                           description: cityViewModel.date)
                            .shadow(radius: 0)
                        
                        WeatherTodayView(cityViewModel: cityViewModel)
                        
                        WeatherSectionView(title: "Forecast", description: "5 days / 3 hours")

                        WeatherForecastView(cityViewModel: cityViewModel)
                    }
                    .padding(.bottom, 30)
                }
            }
            .padding(.top, 40)
        }
        .padding()
        .background(LinearGradient(gradient: Gradient(
            colors: [Color.cyan.opacity(0.5), Color.cyan]),
                                   startPoint: .top,
                                   endPoint: .bottom))
        .edgesIgnoringSafeArea(.all)
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
