//
//  WeatherForecastView.swift
//  WeatherApp
//
//  Created by Adrianna Siwik on 08/05/2023.
//

import SwiftUI

struct WeatherForecastView: View {
    @ObservedObject var cityViewModel: CityViewModel
    
    var body: some View {
        ForEach(cityViewModel.weather.list) { weather in
            LazyVStack {
                dailyCell(weather: weather)
            }
        }
    }
    
    private func dailyCell(weather: WeatherResponse) -> some View {
        HStack {
            VStack {
                Text(cityViewModel.getDayFor(timestamp: weather.dt).uppercased())
                Text(cityViewModel.getHourFor(timestamp: weather.dt).uppercased())
            }
            
            Spacer()
            
            Text("\(cityViewModel.getTempFor(temp: weather.main.temp_max)) | \(cityViewModel.getTempFor(temp: weather.main.temp_min))")
            
            Spacer()
            
            cityViewModel.getWeatherIconFor(icon: !weather.weather.isEmpty ? weather.weather[0].icon : "sun.fill")
        }
        .foregroundColor(.white)
        .padding(.horizontal, 40)
        .padding(.vertical, 15)
        .background(RoundedRectangle(cornerRadius: 5)
            .fill(LinearGradient(gradient: Gradient(colors: [Color.cyan.opacity(0.5), Color.cyan]),
                                 startPoint: .topLeading,
                                 endPoint: .bottomTrailing)).opacity(0.3))
        .shadow(color: .white.opacity(0.1), radius: 2, x: -2, y: -2)
        .shadow(color: .black.opacity(0.2), radius: 2, x: 2, y: 2)
    }
}

struct WeatherForecastView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherTodayView(cityViewModel: CityViewModel())
    }
}
