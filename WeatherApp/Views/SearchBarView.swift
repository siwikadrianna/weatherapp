//
//  SearchBarView.swift
//  WeatherApp
//
//  Created by Adrianna Siwik on 08/05/2023.
//

import SwiftUI

struct SearchBarView: View {
    @ObservedObject var cityViewModel: CityViewModel
    @State private var searchTerm = "Miami"
    
    var body: some View {
        HStack {
            TextField("", text: $searchTerm)
                .padding(.leading, 20)
            
            Button {
                cityViewModel.getLocation(for: searchTerm)
            } label: {
                ZStack {
                    RoundedRectangle(cornerRadius: 10)
                        .fill(.cyan)
                    Image(systemName: "magnifyingglass")
                }
            }
            .frame(width: 50, height: 50)
            
            Button {
                cityViewModel.requestLocation()
            } label: {
                ZStack {
                    RoundedRectangle(cornerRadius: 10)
                        .fill(.cyan)
                    Image(systemName: "location.fill")
                }
            }
            .frame(width: 50, height: 50)
        }
        .foregroundColor(.white)
        .padding()
        .background(
            ZStack(alignment: .leading) {
                RoundedRectangle(cornerRadius: 10)
                    .fill(.cyan.opacity(0.5))
            }
        )
    }
}

struct SearchBarView_Previews: PreviewProvider {
    static var previews: some View {
        SearchBarView(cityViewModel: CityViewModel())
    }
}
