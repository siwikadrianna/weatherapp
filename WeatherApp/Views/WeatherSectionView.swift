//
//  WeatherSectionView.swift
//  WeatherApp
//
//  Created by Adrianna Siwik on 08/05/2023.
//

import SwiftUI

struct WeatherSectionView: View {
    var title: String
    var description: String
    
    var body: some View {
        HStack {
            VStack(alignment: .center) {
                Text(title)
                    .font(.largeTitle)
                    .bold()
                Text(description)
            }
            .foregroundColor(.white)
        }
    }
}

struct WeatherSectionView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherSectionView(title: "Miami", description: "22/02/2022")
    }
}
